#!/bin/bash
echo "Running pre-commit hook"

echo "Running unittests"
./project/scripts/run-unittests.sh

if [ $? -ne 0 ]; then
	echo "Unit tests must pass before commit"
	exit 1
fi

echo "unittests succeeded"

echo "TODO ... Run deployment tests"
#./project/scripts/run-deploymenttests.sh
#TODO check results of deployment tests
